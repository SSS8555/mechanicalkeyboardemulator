;###########################################################################################
;# Directives                                                                              #
;###########################################################################################
#SingleInstance force
#Persistent
#InstallKeybdHook
#InstallMouseHook
#MaxHotkeysPerInterval 200
;###########################################################################################
;# Main Settings & Init                                                                    #
;###########################################################################################
#UseHook On

pressed := {}

return

#include all_keysUD.ahki
~SC136:: ; RShift 
~*SC136::keyDown(54 + 256)
~*SC136 Up::keyUp(54 + 256)

F12::ExitApp
;----------------------------
keyDown(code)
{
    local
    global pressed
    if (Not pressed[code])
    {
;        SoundBeep, 1050, 2
        SoundPlay, click.wav
    }
    pressed[code]:=1
}
;----------------------------
keyUp(code)
{
    local
    global pressed
    pressed[code]:=0
;    SoundBeep, 1250, 2
    SoundPlay, click.wav
}
;----------------------------
